# 指定基础镜像，这是分阶段构建的前期阶段
FROM openjdk:11-jre   as builder
# 执行工作目录
WORKDIR apps
# 配置参数
ARG JAR_FILE=target/*.jar
# 将编译构建得到的jar文件复制到镜像空间中
COPY ${JAR_FILE} application.jar
# 通过工具spring-boot-jarmode-layertools从application.jar中提取拆分后的构建结果
RUN java -Djarmode=layertools -jar application.jar extract

# 正式构建镜像
FROM openjdk:11-jre
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone
WORKDIR apps
# 前一阶段从jar中提取除了多个文件，这里分别执行COPY命令复制到镜像空间中，每次COPY都是一个layer
COPY --from=builder apps/dependencies/ ./
RUN true
COPY --from=builder apps/spring-boot-loader/ ./
RUN true
COPY --from=builder apps/snapshot-dependencies/ ./
RUN true
COPY --from=builder apps/application/ ./
RUN true
ENTRYPOINT java ${JAVA_OPTS} org.springframework.boot.loader.JarLauncher
